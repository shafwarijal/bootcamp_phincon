const menuToggle = document.querySelector(".navbar-menu input");
const onNav = document.querySelector(".navbar-mb-menu");

menuToggle.addEventListener("click", function () {
  onNav.classList.toggle("slide");
});

let sections = document.querySelectorAll("section");
let navLinks = document.querySelectorAll(
  ".navbar-menu .navbar-list ul li .navbar-list-all"
);
let navLinksMb = document.querySelectorAll(
  ".navbar-mb-menu ul li .navbar-list-all"
);

window.onscroll = () => {
  sections.forEach((sec) => {
    let top = window.scrollY;
    let offset = sec.offsetTop - 150;
    let height = sec.offsetHeight;
    let id = sec.getAttribute("id");

    if (top >= offset && top < offset + height) {
      navLinks.forEach((links) => {
        links.classList.remove("nav-active");
        document
          .querySelector(
            ".navbar-menu .navbar-list ul li .navbar-list-all[href*=" + id + "]"
          )
          .classList.add("nav-active");
      });
    }

    if (top >= offset && top < offset + height) {
      navLinksMb.forEach((mblinks) => {
        mblinks.classList.remove("nav-active");
        document
          .querySelector(
            ".navbar-mb-menu ul li .navbar-list-all[href*=" + id + "]"
          )
          .classList.add("nav-active");
      });
    }
  });
};
