import Countdown from "react-countdown";
import { useSearchParams } from "react-router-dom";
import { useRef } from "react";

import "./App.css";
import facebook from "./assets/icon-facebook.svg";
import pinterest from "./assets/icon-pinterest.svg";
import instagram from "./assets/icon-instagram.svg";

function App() {
  const defaultDate = "2023-08-17:00:00";
  const [searchParams] = useSearchParams();
  const date = searchParams.get("date");
  const dateRef = useRef(new Date(date || defaultDate));

  const renderer = ({ days, hours, minutes, seconds }) => {
    return (
      <div className="main">
        <div className="day">
          <div className="box-number">
            <div className="half-left"></div>
            <div className="box-atas"></div>
            <div className="number-title">{days}</div>
            <div className="box-bawah"></div>
            <div className="half-right"></div>
          </div>
          <div className="text-title">Days</div>
        </div>
        <div className="hour">
          <div className="box-number">
            <div className="half-left"></div>
            <div className="box-atas"></div>
            <div className="number-title">{hours}</div>
            <div className="box-bawah"></div>
            <div className="half-right"></div>
          </div>
          <div className="text-title">Hours</div>
        </div>
        <div className="minute">
          <div className="box-number">
            <div className="half-left"></div>
            <div className="box-atas"></div>
            <div className="number-title">{minutes}</div>
            <div className="box-bawah"></div>
            <div className="half-right"></div>
          </div>
          <div className="text-title">Minutes</div>
        </div>
        <div className="second">
          <div className="box-number">
            <div className="half-left"></div>
            <div className="box-atas"></div>
            <div className="number-title">{seconds}</div>
            <div className="box-bawah"></div>
            <div className="half-right"></div>
          </div>
          <div className="text-title">Seconds</div>
        </div>
      </div>
    );
  };

  return (
    <>
      <div className="container">
        <div className="text1">WE&apos;RE LAUNCHING SOON</div>
        <Countdown date={dateRef.current} renderer={renderer} />
        <div className="social">
          <div className="facebook">
            <a href="">
              <img src={facebook} alt="" />
            </a>
          </div>
          <div className="pinterest" href>
            <a href="">
              <img src={pinterest} alt="" />
            </a>
          </div>
          <div className="instagram">
            <a href="">
              <img src={instagram} alt="" />
            </a>
          </div>
        </div>
        <div className="bg-hills"></div>
      </div>
    </>
  );
}

export default App;
