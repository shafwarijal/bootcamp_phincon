import DetailCountry from "./pages/base/detailCountry.jsx";
import MainContry from "./pages/base/mainCountry.jsx";
import { BrowserRouter, Route, Routes } from "react-router-dom";

function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<MainContry />} />
          <Route path="/countries/:nameCountry" element={<DetailCountry />} />
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
