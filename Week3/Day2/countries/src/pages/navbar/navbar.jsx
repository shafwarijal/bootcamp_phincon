import navScss from "./navbar.module.scss";

import PropTypes from "prop-types";
import DarkModeIcon from "@mui/icons-material/DarkMode";
import LightModeIcon from "@mui/icons-material/LightMode";

const Navbar = ({ themeLight, onClickTheme }) => {
  const handleClick = () => {
    if (onClickTheme) {
      onClickTheme();
    }
  };

  return (
    <>
      <div className={`${navScss.navbar} ${themeLight ? "" : navScss.darkNav}`}>
        <div className={navScss.logo}>Where in the world?</div>
        <div className={navScss.darkmode}>
          <button type="button" onClick={handleClick}>
            <div className={navScss.icon}>
              {themeLight ? <LightModeIcon /> : <DarkModeIcon />}
            </div>
            <div className={navScss.title}>
              {themeLight ? "Light Mode" : "Dark Mode"}
            </div>
          </button>
        </div>
      </div>
    </>
  );
};

Navbar.propTypes = {
  onClickTheme: PropTypes.func.isRequired,
  themeLight: PropTypes.bool,
};
export default Navbar;
