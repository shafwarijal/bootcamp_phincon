// App.js
import { useState, useMemo } from "react";
import { ThemeProvider, CssBaseline, createTheme } from "@mui/material";
//
import Content from "../../pages/content/content.jsx";
import Navbar from "../../pages/navbar/navbar.jsx";

const MainContry = () => {
  const [themeLight, setThemeType] = useState(true);

  const theme = useMemo(() => {
    const mainPalette = {
      primary: {
        main: "#6366f1",
      },
      background: {
        default: "#f1f5f9",
      },
    };

    const darkModePalette = {
      primary: {
        main: "#4f46e5",
      },
      background: {
        default: "#1e293b",
      },
    };

    return createTheme({
      palette: {
        mode: themeLight ? "light" : "dark",
        ...mainPalette,
        ...(themeLight ? {} : darkModePalette),
      },
    });
  }, [themeLight]);

  const handleThemeChange = () => {
    setThemeType(!themeLight);
  };

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Navbar onClickTheme={handleThemeChange} themeLight={themeLight} />
      <Content />
    </ThemeProvider>
  );
};

export default MainContry;
