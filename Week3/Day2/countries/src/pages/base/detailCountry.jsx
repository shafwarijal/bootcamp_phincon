import { useState, useMemo, useEffect } from "react";
import { ThemeProvider, CssBaseline, createTheme } from "@mui/material";

//
import Navbar from "../../pages/navbar/navbar.jsx";
import { useParams } from "react-router-dom";
import axios from "axios";
import Detail from "../detail/detail.jsx";

const DetailCountry = () => {
  const [countryDetail, setCountryDetail] = useState([]);
  const { nameCountry } = useParams();
  const [loading, setLoading] = useState(true);
  const [themeLight, setThemeType] = useState(true);

  useEffect(() => {
    const fetchCountryDetail = async () => {
      try {
        const response = await axios.get(
          `https://restcountries.com/v3.1/name/${nameCountry}`
        ); // Replace with your API endpoint

        setCountryDetail(response.data[0]);
        setLoading(false);
      } catch (error) {
        console.error(error);
      }
    };
    fetchCountryDetail();
  }, [nameCountry]);

  //   const theme = createTheme({
  //     palette: {
  //       mode: themeLight ? "dark" : "light",
  //       primary: {
  //         main: themeLight ? "#000" : "#fff", // Warna teks di dark mode (putih)
  //       },
  //       background: {
  //         default: themeLight ? "#2b3642" : "#fff",
  //         paper: themeLight ? "#2b3642" : "#fff",
  //       },
  //     },
  //     components: {
  //       MuiAppBar: {
  //         styleOverrides: {
  //           root: {
  //             backgroundColor: themeLight ? "#2b3642" : "#fff", // Warna latar belakang navbar di dark mode (#2b3642) dan light mode (putih)
  //             color: themeLight ? "#fff" : "#000", // Warna teks navbar di dark mode (putih) dan light mode (hitam)
  //           },
  //         },
  //       },
  //       MuiButton: {
  //         styleOverrides: {
  //           root: {
  //             background: themeLight ? "#2b3642" : "#fff", // Warna latar belakang button di dark mode (#2b3642) dan light mode (putih)
  //           },
  //         },
  //       },
  //     },
  //   });

  const theme = useMemo(() => {
    const mainPalette = {
      primary: {
        main: "#6366f1",
      },
      background: {
        default: "#f1f5f9",
      },
    };

    const darkModePalette = {
      primary: {
        main: "#4f46e5",
      },
      background: {
        default: "#1e293b",
      },
    };

    return createTheme({
      palette: {
        mode: themeLight ? "light" : "dark",
        ...mainPalette,
        ...(themeLight ? {} : darkModePalette),
      },
      components: {
        MuiButton: {
          styleOverrides: {
            root: {
              background: themeLight ? "#fff" : "#2b3642",
              color: themeLight ? "#2b3642" : "#fff",
              width: "9rem", // Warna latar belakang button di dark mode (#2b3642) dan light mode (putih)
              height: "2.2rem",
            },
          },
        },
      },
    });
  }, [themeLight]);

  const handleThemeChange = () => {
    setThemeType(!themeLight);
  };

  // const getValue = (obj, category) => {
  //   if (obj) {
  //     return Object.keys(obj)
  //       .map((item) => {
  //         if (category) {
  //           return obj[item][category];
  //         }
  //         return obj[item];
  //       })
  //       .join(", ");
  //   }
  //   return null;
  // };

  const nativeName = countryDetail?.name?.nativeName
    ? Object.values(countryDetail?.name?.nativeName)
    : [];

  const commonNative = nativeName.map((itemNative) => itemNative?.common);

  const currencies = countryDetail.currencies
    ? Object.keys(countryDetail.currencies)
    : [];

  const languages = countryDetail?.languages
    ? Object.values(countryDetail?.languages)
    : [];

  const bordersList = countryDetail.borders ? countryDetail.borders : [];

  return (
    <>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Navbar onClickTheme={handleThemeChange} themeLight={themeLight} />

        <Detail
          loading={loading}
          countryDetail={countryDetail}
          themeLight={themeLight}
          currencies={currencies}
          languages={languages}
          bordersList={bordersList}
          commonNative={commonNative}
        />
      </ThemeProvider>
    </>
  );
};

export default DetailCountry;
