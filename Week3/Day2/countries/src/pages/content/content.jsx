import { useEffect, useState, useMemo } from "react";

import contentScss from "./content.module.scss";
import TextField from "@mui/material/TextField";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import Skeleton from "@mui/material/Skeleton";
import Grid from "@mui/material/Grid";
import { Link } from "react-router-dom";
import {
  PiSortAscendingThin,
  PiSortDescendingThin,
  // TbSortDescendingLetters,
} from "react-icons/pi";
import IconButton from "@mui/material/IconButton";
import Pagination from "@mui/material/Pagination";

import { CardActionArea, styled } from "@mui/material";
import Box from "@mui/material/Box";
import axios from "axios";

const Content = () => {
  const [region, setRegion] = useState("All");
  const [countries, setCountries] = useState([]);
  const [search, setSearch] = useState("");
  const [sortType, setSortType] = useState("ascending");
  const [loading, setLoading] = useState(true);
  const [pageNow, setPageNow] = useState(1);
  const maxPage = 8;

  // const [regionCountryList, setRegionCountryList] = useState([]);
  useEffect(() => {
    fetchCountries();
  }, []);

  const fetchCountries = async () => {
    try {
      // setLoading(true);
      // const response = await axios.get("https://api.example.com/data", {
      //   timeout: 5000, // Set the timeout in milliseconds (5 seconds in this example)
      // });
      const response = await axios.get("https://restcountries.com/v3.1/all"); // Replace with your API endpoint
      // const sortedCountries = response.data.sort((a, b) =>
      //   a.name.common.localeCompare(b.name.common)
      // );
      setCountries(response.data);
      setLoading(false);
    } catch (error) {
      console.error(error);
    }
  };

  const sortedCountries = useMemo(() => {
    let result = countries;

    if (sortType === "descending") {
      result = [...countries].sort((a, b) => {
        return b.name.common.localeCompare(a.name.common);
      });
    } else if (sortType === "ascending") {
      result = [...countries].sort((a, b) => {
        return a.name.common.localeCompare(b.name.common);
      });
    }

    return result;
  }, [countries, sortType]);

  const handleChange = (event) => {
    setRegion(event.target.value);
    setPageNow(1);
  };

  const handleSearchChange = (event) => {
    setSearch(event.target.value);
    setPageNow(1);
  };

  const handleSort = () => {
    setSortType((prevSortType) =>
      prevSortType === "ascending" ? "descending" : "ascending"
    );
    setPageNow(1);
  };

  const handlePage = (event, page) => {
    setPageNow(page);
  };

  // setRegionCountryList(
  //   countries.filter((item) => item.region === event.target.value)
  // );
  const filteredCountries = sortedCountries.filter(
    (country) =>
      country.name.common.toLowerCase().includes(search.toLowerCase()) &&
      (region === "All" ||
        country.region.toLowerCase() === region.toLowerCase())
  );

  // const sliceCountry = filteredCountries.slice(0, 8);
  const lastCountry = pageNow * maxPage;
  const firstCountry = lastCountry - maxPage;
  const pageCountry = filteredCountries.slice(firstCountry, lastCountry);
  const listCountreis = Math.ceil(filteredCountries.length / maxPage);

  const CustomCard = styled(Card)(({ theme }) => ({
    backgroundColor: theme.palette.mode === "dark" ? "#334155" : "#fff",
    color: theme.palette.mode === "dark" ? "#fff" : "#000",
    "&:hover": {
      backgroundColor: theme.palette.mode === "dark" ? "#334155" : "#fff",
      color: theme.palette.mode === "dark" ? "#fff" : "#000",
    },
  }));

  const styles = {
    media: {
      objectFit: "fill",
    },
  };

  return (
    <>
      <div className={contentScss.content}>
        <div className={contentScss.top}>
          <div className={contentScss.topSearch}>
            <Box
              component="form"
              sx={{
                "& > :not(style)": { width: "50ch" },
              }}
              noValidate
              autoComplete="off"
            >
              <TextField
                className={contentScss.textFieldCustom}
                id="outlined-basic"
                label="search for a country..."
                variant="outlined"
                value={search}
                onChange={handleSearchChange}
              />
            </Box>
          </div>
          <div className={contentScss.topRight}>
            <div className={contentScss.topSorting}>
              <IconButton
                className={contentScss.btn}
                variant="contained"
                color="inherit"
                onClick={handleSort}
              >
                {sortType === "ascending" ? (
                  <PiSortAscendingThin size={"2rem"} />
                ) : (
                  <PiSortDescendingThin size={"2rem"} />
                )}
              </IconButton>
            </div>
            <div className={contentScss.topDropdown}>
              <Box>
                <FormControl fullWidth className={contentScss.test2}>
                  <InputLabel id="demo-simple-select-label">
                    Filter by Region
                  </InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={region}
                    label="Filter by Region"
                    onChange={handleChange}
                  >
                    <MenuItem value={"All"}>All Regions</MenuItem>
                    <MenuItem value={"Africa"}>Africa</MenuItem>
                    <MenuItem value={"Americas"}>America</MenuItem>
                    <MenuItem value={"Asia"}>Asia</MenuItem>
                    <MenuItem value={"Europe"}>Europe</MenuItem>
                    <MenuItem value={"Oceania"}>Oceania</MenuItem>
                  </Select>
                </FormControl>
              </Box>
            </div>
          </div>
        </div>
        <div className={contentScss.main}>
          {loading
            ? Array.from({ length: 8 }).map((_, index) => (
                <Grid item xs={2} sm={4} md={3} key={index}>
                  <Box sx={{ width: "100%" }}>
                    <Skeleton
                      animation="wave"
                      variant="rounded"
                      height={140}
                      width={350}
                    />
                    <Skeleton
                      animation="wave"
                      variant="text"
                      sx={{ fontSize: "1rem" }}
                    />
                    <Skeleton
                      animation="wave"
                      variant="text"
                      sx={{ fontSize: "1rem" }}
                      width="60%"
                    />
                    <Skeleton
                      animation="wave"
                      variant="text"
                      sx={{ fontSize: "1rem" }}
                      width="60%"
                    />
                  </Box>
                </Grid>
              ))
            : pageCountry?.map((data, i) => (
                <div className={contentScss.card} key={i}>
                  <CustomCard sx={{ width: "100%" }}>
                    <Link
                      to={`/countries/${data.name.common}`}
                      style={{ textDecoration: "none", color: "inherit" }}
                    >
                      <CardActionArea>
                        <CardMedia
                          component="img"
                          height="180"
                          image={data.flags.png}
                          alt=""
                          style={styles.media}
                        />
                        <CardContent>
                          <Typography gutterBottom variant="h5" component="div">
                            {data.name.common}
                          </Typography>
                          <Typography variant="body2" color="text.secondary">
                            <b>Population:</b>{" "}
                            {data.population.toLocaleString("en-US")}
                          </Typography>
                          <Typography variant="body2" color="text.secondary">
                            <b>Region:</b> {data.region}
                          </Typography>
                          <Typography variant="body2" color="text.secondary">
                            <b>Capital:</b> {data.capital}
                          </Typography>
                          {/* <div className={contentScss.countryName}>
                      {data.name.common}
                    </div>
                    <div className={contentScss.population}>
                      <span>Population : </span>
                      {data.population}
                    </div>
                    <div className={contentScss.region}>
                      <span>Region : </span>
                      {data.region}
                    </div>
                    <div
                      className={`${contentScss.capital} ${
                        themeLight ? "" : contentScss.capital1
                      }`}
                    >
                      <span>Capital : </span>
                      {data.capital}
                    </div> */}
                        </CardContent>
                      </CardActionArea>
                    </Link>
                  </CustomCard>
                </div>
              ))}
        </div>
        {/* <Box sx={{ display: "flex", justifyContent: "center", mt: 5 }}>
          <Pagination
            color="primary"
            count={10}
            page={3}
            boundaryCount={2}
            renderItem={(item) => (
              <PaginationItem
                slots={{ previous: ArrowBackIcon, next: ArrowForwardIcon }}
                {...item}
              />
            )}
          />
        </Box> */}
        <Box sx={{ display: "flex", justifyContent: "center" }}>
          <Pagination
            page={pageNow}
            count={listCountreis}
            size="large"
            onChange={handlePage}
          />
        </Box>
      </div>
    </>
  );
};
export default Content;
