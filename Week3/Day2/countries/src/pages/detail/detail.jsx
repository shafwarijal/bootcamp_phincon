/* eslint-disable react/prop-types */
import detailScss from "./country.module.scss";

import { Link } from "react-router-dom";
import { Button } from "@mui/material";
import Skeleton from "@mui/material/Skeleton";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";

const Detail = ({
  loading,
  countryDetail,
  themeLight,
  commonNative,
  currencies,
  languages,
  bordersList,
}) => {
  console.log(loading);
  return (
    <>
      <div className={detailScss.detail}>
        <Button
          className={detailScss.btn}
          component={Link}
          to="/"
          variant="contained"
          color="inherit"
          startIcon={<ArrowBackIcon />}
        >
          Back
        </Button>

        {loading ? (
          <div className={detailScss.detailItem}>
            <Skeleton
              animation="wave"
              variant="rounded"
              height="50vh"
              width="100%"
            />
            <div className={detailScss.desc1}>
              <Skeleton
                animation="wave"
                variant="rounded"
                height="8vh"
                width="100%"
              />
              <Skeleton
                animation="wave"
                variant="rounded"
                height="20vh"
                width="100%"
              />
              <Skeleton
                animation="wave"
                variant="rounded"
                height="5vh"
                width="100%"
              />
            </div>
            {/* <Box sx={{ width: "100%" }}>
              
            </Box> */}
          </div>
        ) : (
          <div className={detailScss.detailItem}>
            <img
              src={countryDetail.flags?.svg}
              alt=""
              className={`${detailScss.flag} ${
                themeLight ? "" : detailScss.flag1
              }`}
            />
            <div className={detailScss.desc}>
              <div className={detailScss.nameDesc}>
                {countryDetail?.name?.common}
              </div>
              <div className={detailScss.boxDesc}>
                <div className={detailScss.leftDesc}>
                  <div className={detailScss.nativeName}>
                    <b>Native Name : </b>
                    {commonNative.join(", ")}
                  </div>
                  <div className={detailScss.population}>
                    <b>Population : </b>
                    {countryDetail?.population?.toLocaleString("en-US")}
                  </div>
                  <div className={detailScss.region}>
                    <b>Region : </b> {countryDetail?.region}
                  </div>
                  <div className={detailScss.subRegion}>
                    <b>Sub Region : </b> {countryDetail?.subregion}
                  </div>
                  <div className={detailScss.capital}>
                    <b>Capital : </b>
                    {countryDetail?.capital}
                  </div>
                </div>
                <div className={detailScss.rightDesc}>
                  <div className={detailScss.tld}>
                    <b>Top Level Domain : </b> {countryDetail?.tld?.join(", ")}
                  </div>
                  <div className={detailScss.currencies}>
                    <b>Currencies : </b>
                    {currencies}
                  </div>
                  <div className={detailScss.languages}>
                    <b>Languages : </b> {languages.join(", ")}
                  </div>
                </div>
              </div>
              <div className={detailScss.border}>
                <b>Border Countries : </b>
                <div className={detailScss.boxBtn}>
                  {bordersList?.map((item, i) => (
                    <Button
                      className={detailScss.btn1}
                      variant="contained"
                      color="inherit"
                      key={i}
                    >
                      {item}
                    </Button>
                  ))}
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    </>
  );
};

export default Detail;
