//Show list title
import fetch from "node-fetch";

const fetchTitles = async () => {
  const response = await fetch(
    "https://api.jikan.moe/v4/recommendations/anime"
  ); // Replace with your API endpoint
  const body = await response.json();
  const dataTitle = body.data;

  const limited = dataTitle.slice(0, 20);

  const resultDate = limited.map((x) => x.entry.map((y) => y.title));

  const flatTitle = resultDate.flat();
  console.log(
    "==============================List Title=============================="
  );
  console.table(flatTitle);
};

const fetchTitlesSorted = async () => {
  const response = await fetch(
    "https://api.jikan.moe/v4/recommendations/anime"
  ); // Replace with your API endpoint
  const body = await response.json();
  const dataTitle = body.data;

  const limited = dataTitle.slice(0, 20);

  // const flatDate = limited.map((a) => a.date);

  const sorted = limited.sort((a, b) => new Date(a.date) - new Date(b.date));

  const resultDate = sorted.map((x) => {
    const getTitle = x.entry.map((y) => y.title);
    const getDate = x.date;
    const marged = getTitle.flat();
    // return marged;
    return { TitleSorted: marged, Date: getDate };
  });

  // const resultDateMarge = resultDate.flat();
  console.log(
    "==========================List Title Sorted=========================="
  );
  console.table(resultDate);
};

const fetchPopular = async () => {
  const response = await fetch(
    "https://api.jikan.moe/v4/recommendations/anime"
  ); // Replace with your API endpoint
  const body = await response.json();
  const dataPopular = body.data;

  const limit = dataPopular.slice(0, 5);

  const resultMalId = limit.map((x) => {
    const getMalId = x.entry.map((y) => y.mal_id);
    const marged = getMalId.flat();
    return marged;
  });

  const flatId = resultMalId.flat();

  // const sliceId = id.slice(0, 5);
  // console.log(id);

  let resultTimeoutArray = new Array();

  // const detailAnime = sliceId.map((aa, i) => {
  //   setTimeout(async () => {
  //     const response = await fetch(`https://api.jikan.moe/v4/anime/${aa}`);
  //     const body = await response.json();
  //     const dataPopularity = body.data;

  //     resultTimeoutArray.push(dataPopularity.popularity);

  //     console.log(resultTimeoutArray);

  //     // return dataPopularity.popularity;
  //   }, 3000 * (i + 1));
  // });

  const myPromise = new Promise((resolve, reject) => {
    flatId.map((id, i) => {
      setTimeout(async () => {
        const response = await fetch(`https://api.jikan.moe/v4/anime/${id}`);
        const body = await response.json();
        const dataPopularity = body.data;

        resultTimeoutArray.push(dataPopularity);
        if (i == flatId.length - 1) {
          resolve(resultTimeoutArray);
        }

        // return dataPopularity.popularity;
      }, 3000 * (i + 1));
    });
  });

  myPromise.then((item) => {
    const sortedPopular = item
      .sort((a, b) => {
        return b.popularity - a.popularity;
      })
      .slice(0, 5)
      .map((a) => {
        const getTitle = a.title;
        const getPopularity = a.popularity;

        return { Title: getTitle, Popularity: getPopularity };
      });
    console.log(
      "==============================Popular=============================="
    );
    console.table(sortedPopular);
  });
};

const fetchRank = async () => {
  const response = await fetch(
    "https://api.jikan.moe/v4/recommendations/anime"
  ); // Replace with your API endpoint
  const body = await response.json();
  const data = body.data;

  const limit = data.slice(0, 5);

  const resultMalId = limit.map((x) => {
    const getMalId = x.entry.map((y) => y.mal_id);
    const marged = getMalId.flat();
    return marged;
  });

  const flatId = resultMalId.flat();
  let resultTimeoutArray = new Array();

  const myPromise = new Promise((resolve, reject) => {
    flatId.map((id, i) => {
      setTimeout(async () => {
        const response = await fetch(`https://api.jikan.moe/v4/anime/${id}`);
        const body = await response.json();
        const dataRank = body.data;

        resultTimeoutArray.push(dataRank);
        if (i == flatId.length - 1) {
          resolve(resultTimeoutArray);
        }
      }, 3000 * (i + 1));
    });
  });

  myPromise.then((item) => {
    const sortedRank = item
      .sort((a, b) => {
        return b.rank - a.rank;
      })
      .slice(0, 5)
      .map((a) => {
        const getTitle = a.title;
        const getRank = a.rank;

        return { Title: getTitle, Rank: getRank };
      });
    console.log(
      "==============================Rank=============================="
    );
    console.table(sortedRank);
  });
};

const fetchMostEpisode = async () => {
  const response = await fetch(
    "https://api.jikan.moe/v4/recommendations/anime"
  ); // Replace with your API endpoint
  const body = await response.json();
  const data = body.data;

  const limit = data.slice(0, 5);

  const resultMalId = limit.map((x) => {
    const getMalId = x.entry.map((y) => y.mal_id);
    const marged = getMalId.flat();
    return marged;
  });

  const flatId = resultMalId.flat();
  let resultTimeoutArray = new Array();

  const myPromise = new Promise((resolve, reject) => {
    flatId.map((id, i) => {
      setTimeout(async () => {
        const response = await fetch(`https://api.jikan.moe/v4/anime/${id}`);
        const body = await response.json();
        const dataEpi = body.data;

        resultTimeoutArray.push(dataEpi);
        if (i == flatId.length - 1) {
          resolve(resultTimeoutArray);
        }
      }, 3000 * (i + 1));
    });
  });

  myPromise.then((item) => {
    const mostEpi = item
      .sort((a, b) => {
        return b.episodes - a.episodes;
      })
      .slice(0, 1)
      .map((a) => {
        const getTitle = a.title;
        const getEpi = a.episodes;

        return { Title: getTitle, Rank: getEpi };
      });
    console.log(
      "==============================Most Episodes=============================="
    );
    console.table(mostEpi);
  });
};

// fetchTitles();
// fetchTitlesSorted();
// fetchMostEpisode();
// fetchRank();
// fetchPopular();
