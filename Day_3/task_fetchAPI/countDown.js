let countDown = 10;

const inter = setInterval(function () {
  let hours = Math.floor((countDown % (60 * 60 * 24)) / (60 * 60));
  let minutes = Math.floor((countDown % (60 * 60)) / 60);
  let seconds = Math.floor(countDown % 60);

  hours = hours < 10 ? "0" + hours : hours;
  minutes = minutes < 10 ? "0" + minutes : minutes;
  seconds = seconds < 10 ? "0" + seconds : seconds;

  console.log(`${hours}:${minutes}:${seconds}`);

  countDown--;

  if (countDown < 0) {
    console.log("Selesai");
    clearInterval(inter);
  }
}, 1000);
