const leftSlide = (i) => {
  let sortedArrFirst = i.sort((a, b) => {
    if (b === 0) return -1;
  });

  for (let i = 0; i < sortedArrFirst.length; i++) {
    if (sortedArrFirst[i] === sortedArrFirst[i + 1]) {
      sortedArrFirst[i] += sortedArrFirst[i + 1];
      sortedArrFirst[i + 1] = 0;
    }
  }

  let sortedArrSec = sortedArrFirst.sort((a, b) => {
    if (b === 0) return -1;
  });

  return sortedArrSec;
};

console.log(leftSlide([2, 2, 2, 0]));
console.log(
  leftSlide([1024, 1024, 1024, 512, 512, 256, 256, 128, 128, 64, 32, 32])
);
