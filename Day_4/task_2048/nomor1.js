const calc = (x, y, z) => {
  let result = 0;
  if (z === "add") {
    result = parseInt(x) + parseInt(y);
  } else if (z === "subtract") {
    result = x - y;
  } else if (z === "divide") {
    result = x / y;
  } else if (z === "multiply") {
    result = x * y;
  }
  return result == Infinity ? undefined : result;
};

console.log(calc("1", "2", "add"));
console.log(calc("4", "5", "subtract"));
console.log(calc("6", "3", "divide"));
console.log(calc("2", "7", "multiply"));
console.log(calc("6", "0", "divide"));
