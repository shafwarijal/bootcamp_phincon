function containsNumber(string) {
  for (var i = 0; i < string.length; i++) {
    if (!isNaN(parseInt(string[i]))) {
      return true;
    }
  }
  return false;
}

function numInStr(arr) {
  var result = [];
  for (var i = 0; i < arr.length; i++) {
    if (containsNumber(arr[i])) {
      result.push(arr[i]);
    }
  }
  return result;
}

console.log(numInStr(["1a", "a", "2b", "b"]));
console.log(numInStr(["abc", "abc10"]));
console.log(numInStr(["abc", "ab10c", "a10bc", "bcd"]));
console.log(numInStr(["this is a test", "test1"]));
console.log(numInStr(["abc", "ab10c", "a10bc", "bcd"]));
console.log(numInStr(["1", "a", " ", "b"]));
console.log(numInStr(["rct", "ABC", "Test", "xYz"]));
