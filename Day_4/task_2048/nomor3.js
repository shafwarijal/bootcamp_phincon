const alphaVal = (s) => s.toLowerCase().charCodeAt(0) - 97 + 1;

const balanced = (word) => {
  let divide = Math.floor(word.length / 2);
  let start = word.slice(0, divide);
  let end =
    word.length % 2 === 0 ? word.slice(divide) : word.slice(-1 * divide);

  let setValStart = 0;
  let setValEnd = 0;

  for (let i = 0; i < start.length; i++) {
    setValStart += alphaVal(start[i]);
    setValEnd += alphaVal(end[i]);
  }

  return setValStart === setValEnd;
};

console.log(balanced("brake"));
console.log(balanced("zips"));
console.log(balanced("at"));
console.log(balanced("forgetful"));
console.log(balanced("vegetation"));
console.log(balanced("disillusioned"));
console.log(balanced("abstract"));
console.log(balanced("clever"));
console.log(balanced("conditionalities"));
console.log(balanced("seasoning"));
