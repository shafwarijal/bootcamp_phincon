const inBox = (arr) => {
  for (let i = 0; i < arr.length; i++) {
    if (
      arr[i][0] === "#" &&
      arr[i][arr[i].length - 1] === "#" &&
      arr[i].includes("*")
    )
      return true;
  }
  return false;
};

console.log(inBox(["###", "#*#", "###"]));
console.log(inBox(["####", "#* #", "#  #", "####"]));
console.log(inBox(["*####", "# #", "#  #*", "####"]));
console.log();
