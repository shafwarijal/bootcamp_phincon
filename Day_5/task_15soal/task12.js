const spiralOrder = (arr) => {
  const spiralArr = [
    ...arr[0],
    arr[1][arr[1].length - 1],
    ...arr[2].reverse(),
    ...arr[1],
  ];
  const setSpiral = [...new Set([...spiralArr])];

  return setSpiral;
};

console.log(
  spiralOrder([
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
  ])
);
