const missingNum = (str) => {
  let min = 1;
  let max = 10;

  let all = Array.from(Array(max), (x, y) => y + min);
  console.log(all);
  return all.filter((x) => !str.includes(x));
};

console.log(missingNum([7, 2, 3, 6, 5, 9, 1, 4, 8]));
