const removeDups = (str) => {
  const toSet = new Set(str);
  let remove = Array.from(toSet);

  //   const toSet = [...new Set(str)];  //cara yang lebih simple

  return remove;
};

console.log(removeDups(["The", "big", "cat"]));
console.log(removeDups([1, 0, 1, 0]));
console.log(removeDups(["The", "big", "cat"]));
console.log(removeDups(["John", "Taylor", "John"]));
console.log(removeDups(["John", "Taylor", "John", "john"]));
console.log(
  removeDups([
    "javascript",
    "python",
    "python",
    "ruby",
    "javascript",
    "c",
    "ruby",
  ])
);
console.log(removeDups([1, 2, 2, 2, 3, 2, 5, 2, 6, 6, 3, 7, 1, 2, 5]));
console.log(removeDups(["#", "#", "%", "&", "#", "$", "&"]));
console.log(removeDups([3, "Apple", 3, "Orange", "Apple"]));
