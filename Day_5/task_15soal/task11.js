// player A = Abigail
// player B = Benson

const calculateScore = (item) => {
  let scoreA = 0;
  let scoreB = 0;

  for (let i = 0; i < item.length; i++) {
    if (item[i][0] === "R" && item[i][1] === "R") {
      scoreA += 0;
    } else if (item[i][0] === "R" && item[i][1] === "S") {
      scoreA += 1;
    } else if (item[i][0] === "R" && item[i][1] === "P") {
      scoreB += 1;
    } else if (item[i][0] === "P" && item[i][1] === "P") {
      scoreA += 0;
    } else if (item[i][0] === "P" && item[i][1] === "R") {
      scoreA += 1;
    } else if (item[i][0] === "P" && item[i][1] === "S") {
      scoreB += 1;
    } else if (item[i][0] === "S" && item[i][1] === "S") {
      scoreA += 0;
    } else if (item[i][0] === "S" && item[i][1] === "P") {
      scoreA += 1;
    } else if (item[i][0] === "S" && item[i][1] === "R") {
      scoreB += 1;
    }
  }

  if (scoreA === scoreB) return "Tie";
  if (scoreA > scoreB) return "Abigail";
  return "Benson";
};

console.log(
  calculateScore([
    ["R", "P"],
    ["R", "S"],
    ["S", "P"],
  ])
);

console.log(
  calculateScore([
    ["R", "R"],
    ["S", "S"],
  ])
);
