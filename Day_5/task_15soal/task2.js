const makeTitle = (str) => {
  let arr = str.split(" ");
  let newArr = [];

  for (let i = 0; i < arr.length; i++) {
    newArr.push(arr[i].charAt(0).toUpperCase() + arr[i].slice(1));
  }
  return newArr.join(" ");
};

console.log(makeTitle("This is a title"));
console.log(makeTitle("capitalize every word"));
console.log(makeTitle("I Like Pizza"));
console.log(makeTitle("PIZZA PIZZA PIZZA"));
console.log(makeTitle("I am a title"));
console.log(makeTitle("I AM A TITLE"));
console.log(makeTitle("i aM a tITLE"));
