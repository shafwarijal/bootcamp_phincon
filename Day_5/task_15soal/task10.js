function numberPairs(numbers) {
  const numberArr = numbers.split(" ").map(Number);

  const numberArrSlice = numberArr.slice(1);
  const numberCount = {};

  for (let i = 0; i < numberArrSlice.length; i++) {
    const num = numberArrSlice[i];
    if (numberCount[num]) {
      numberCount[num]++;
    } else {
      numberCount[num] = 1;
    }
  }

  let pairCount = 0;
  for (const num in numberCount) {
    if (numberCount[num] >= 2) {
      pairCount += Math.floor(numberCount[num] / 2);
    }
  }

  return pairCount;
}

const input = "4 2 3 4 1 4";
const result = numberPairs(input);
console.log(result);

// const number_pairs = (arr) => {
//   numArr = arr.split(" ").map(Number);

//   return numArr;
// };

// console.log(number_pairs("7 1 2 1 2 1 3 2"));

// const array1 = [1, 2, 3, 4, 4];

// // 0 + 1 + 2 + 3 + 4
// const initialValue = 0;
// let sama = 0;
// const sumWithInitial = array1.reduce((accumulator, currentValue) => {
//   if (array1[accumulator] == array1[currentValue]) {
//     sama += 2;
//   }
//   return sama;
// }, initialValue);

// console.log(sumWithInitial);
