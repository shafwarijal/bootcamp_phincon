const emotify = (a) => {
  let word = a.split(" ")[2];
  let change = "";
  if (word === "smile") {
    change = ":D";
  } else if (word === "grin") {
    change = ":(";
  } else if (word === "sad") {
    change = ":(";
  } else if (word === "mad") {
    change = ":P";
  }
  return "Make me " + change;
};

console.log(emotify("Make me smile"));
