const awardPrizes = (arr) => {
  let trueArr = Object.entries(arr);
  let toArr = Object.entries(arr).sort((a, b) => b[1] - a[1]);
  let award = {};

  for (let i = 0; i < toArr.length; i++) {
    const [name1] = trueArr[i];

    if (trueArr[i][0] === toArr[0][0]) {
      award[name1] = "Gold";
    } else if (trueArr[i][0] === toArr[1][0]) {
      award[name1] = "Silver";
    } else if (trueArr[i][0] === toArr[2][0]) {
      award[name1] = "Bronze";
    } else {
      award[name1] = "Participation";
    }
  }
  return award;
};

console.log(
  awardPrizes({
    Joshua: 45,
    Alex: 39,
    Eric: 43,
  })
);

console.log(
  awardPrizes({
    Mario: 99,
    Luigi: 100,
    Yoshi: 299,
    Toad: 2,
  })
);
