const reverseWords = (str) => {
  return str.split(" ").reverse().join(" ").replace(/\s+/g, " ").trim();
};

console.log(reverseWords("  the sky is blue"));
console.log(reverseWords("hello   world!  "));
console.log(reverseWords("a good example"));
console.log(reverseWords("hello world!"));
