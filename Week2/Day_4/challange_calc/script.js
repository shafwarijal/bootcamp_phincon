const display = document.querySelector(".display");
const buttons = document.querySelectorAll(".button_calc button");
const specialChars = ["*", "/", "-", "+", "="];
let output = "";
//Define function to calculate based on button clicked.
const calculate = (btnValue) => {
  console.log(btnValue);

  display.focus();
  if (btnValue === "=" && output !== "") {
    //If output has '%', replace with '/100' before evaluating.
    output = eval(output.replace("%", "/100"));
  } else if (btnValue === "RESET") {
    output = "";
  } else if (btnValue === "DEL") {
    //If DEL button is clicked, remove the last character from the output.
    output = output.toString().slice(0, -1);
  } else {
    //If output is empty and button is specialChars then return
    if (output === "" && specialChars.includes(btnValue)) return;
    output += btnValue;
  }
  display.value = output;
};
//Add event listener to buttons, call calculate() on click.
buttons.forEach((button) => {
  //Button click listener calls calculate() with dataset value as argument.
  button.addEventListener("click", (e) => calculate(e.target.dataset.value));
});

let toggleSwitch = document.getElementsByClassName("redButton")[0];
function go_to_1() {
  toggleSwitch.classList.add("horizTranslate1");
  toggleSwitch.classList.remove("horizTranslate2");
  toggleSwitch.classList.remove("horizTranslate3");
  var containerElement = document.getElementById("container");
  containerElement.style = "background-color: rgb(44, 81, 112);";
  var textSwitchElement = document.getElementById("text-switch");
  textSwitchElement.style = "color: #fcfcfd;";
  var text2SwitchElement = document.getElementById("text2-switch");
  text2SwitchElement.style = "color: #fcfcfd;";
  var legendTextContainerElement = document.getElementById(
    "legendTextContainer"
  );
  legendTextContainerElement.style = "#ffffff;";
  var resultElement = document.getElementById("result");
  resultElement.style = "background: rgb(25, 44, 59);";
  var resultTextElement = document.getElementById("resultText");
  resultTextElement.style = "color: #fcfcfd;";
  var buttonCalcElement = document.getElementById("button_calc");
  buttonCalcElement.style = "background: rgb(39, 65, 85);";
  var buttonContainerElement = document.getElementById("buttonContainer");
  buttonContainerElement.style = "background: rgb(39, 65, 85);";
  var buttonElement = document.getElementsByClassName("btn");
  for (let i = 0; i < buttonElement.length; i++) {
    if (i == "3") {
      buttonElement[i].style = "background-color: rgb(79, 123, 160);";
    } else if (i == "16") {
      buttonElement[i].style = "background-color: rgb(79, 123, 160);";
    } else if (i == "17") {
      buttonElement[i].style = "background-color: rgb(207, 45, 45)";
    } else {
      buttonElement[i].style = "background-color: #fcfcfd;";
    }
  }

  // document.getElementById("outerContainer").style.backgroundColor = "#4A5B7E";
  // document.getElementById("buttonContainer").style.backgroundColor = "#222D41";
  // document.getElementById("legendTextContainer").style.color = "#ffffff";
}

function go_to_2() {
  toggleSwitch.classList.add("horizTranslate2");
  toggleSwitch.classList.remove("horizTranslate3");
  toggleSwitch.classList.remove("horizTranslate1");
  var containerElement = document.getElementById("container");
  containerElement.style = "background-color: rgb(236, 236, 236);";
  var textSwitchElement = document.getElementById("text-switch");
  textSwitchElement.style = "color: #000000;";
  var text2SwitchElement = document.getElementById("text2-switch");
  text2SwitchElement.style = "color: #000000;";
  var legendTextContainerElement = document.getElementById(
    "legendTextContainer"
  );
  legendTextContainerElement.style = "color: #000000;";
  var resultElement = document.getElementById("result");
  resultElement.style = "background: rgb(255, 255, 255);";
  var resultTextElement = document.getElementById("resultText");
  resultTextElement.style = "color: #000000;";
  var buttonCalcElement = document.getElementById("button_calc");
  buttonCalcElement.style = "background: rgb(214, 214, 214);";
  var buttonContainerElement = document.getElementById("buttonContainer");
  buttonContainerElement.style = "background: rgb(214, 214, 214);";
  var buttonElement = document.getElementsByClassName("btn");
  for (let i = 0; i < buttonElement.length; i++) {
    if (i == "3") {
      buttonElement[i].style = "background-color: #29756d;";
    } else if (i == "16") {
      buttonElement[i].style = "background-color: #29756d;";
    } else if (i == "17") {
      buttonElement[i].style = "background-color: #ff6700;";
    } else {
      buttonElement[i].style = "background-color: #fbfbfb;";
    }
  }
  // document.getElementById("outerContainer").style.backgroundColor = "#E5E5E5";
  // document.getElementById("buttonContainer").style.backgroundColor = "#D3CCCA";
  // document.getElementById("legendTextContainer").style.color = "#222222";
}

function go_to_3() {
  toggleSwitch.classList.add("horizTranslate3");
  toggleSwitch.classList.remove("horizTranslate2");
  toggleSwitch.classList.remove("horizTranslate1");
  var containerElement = document.getElementById("container");
  containerElement.style = "background-color: rgb(32, 4, 39);";
  var textSwitchElement = document.getElementById("text-switch");
  textSwitchElement.style = "color: #ffc23d;";
  var text2SwitchElement = document.getElementById("text2-switch");
  text2SwitchElement.style = "color: #ffc23d;";
  var legendTextContainerElement = document.getElementById(
    "legendTextContainer"
  );
  legendTextContainerElement.style = "color: #ffc23d;";
  var resultElement = document.getElementById("result");
  resultElement.style = "background: rgb(52, 15, 61);";
  var resultTextElement = document.getElementById("resultText");
  resultTextElement.style = "color: #ffc23d;";
  var buttonCalcElement = document.getElementById("button_calc");
  buttonCalcElement.style = "background: rgb(52, 15, 61);";
  var buttonContainerElement = document.getElementById("buttonContainer");
  buttonContainerElement.style = "background: rgb(52, 15, 61);";
  var buttonElement = document.getElementsByClassName("btn");
  for (let i = 0; i < buttonElement.length; i++) {
    if (i == "3") {
      buttonElement[
        i
      ].style = `background-color: rgb(123, 14, 150); box-shadow: rgba(81, 66, 117, 0.4) 0 2px 4px,
      rgba(76, 63, 105, 0.3) 0 7px 13px -3px, rgb(185, 9, 230) 0 -3px 0 inset;`;
    } else if (i == "16") {
      buttonElement[
        i
      ].style = `background-color: rgb(123, 14, 150); box-shadow: rgba(81, 66, 117, 0.4) 0 2px 4px,
      rgba(76, 63, 105, 0.3) 0 7px 13px -3px, rgb(185, 9, 230) 0 -3px 0 inset;`;
    } else if (i == "17") {
      buttonElement[
        i
      ].style = `color: black; background-color: rgb(28, 192, 192); box-shadow: rgba(81, 66, 117, 0.4) 0 2px 4px,
      rgba(76, 63, 105, 0.3) 0 7px 13px -3px, rgb(129, 235, 235) 0 -3px 0 inset;`;
    } else {
      buttonElement[
        i
      ].style = `color: #ffc23d; background-color: rgb(82, 35, 94); box-shadow: rgba(81, 66, 117, 0.4) 0 2px 4px,
      rgba(76, 63, 105, 0.3) 0 7px 13px -3px, rgb(119, 19, 145) 0 -3px 0 inset;`;
    }
  }
}
