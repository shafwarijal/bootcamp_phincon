//1

function myFunction(set, val) {
  return set.has(val);
}

//2

function myFunction(set) {
  return Array.from(set);
}

//3

function myFunction(a, b) {
  return new Set([...a, ...b]);
}

//4

function myFunction(a, b, c) {
  let sets = new Set();

  sets.add(a);
  sets.add(b);
  sets.add(c);

  return sets;
}

//5

function myFunction(set, val) {
  set.delete(val);
  return set;
}
