//1

function myFunction(a, n) {
  return a.slice(n - 1, n)[0];
}

//2

function myFunction(a) {
  return a.slice(3);
}

//3

function myFunction(a) {
  return a.slice(-3);
}

//4

function myFunction(a) {
  return a.slice(0, 3);
}

//5

function myFunction(a, n) {
  return a.slice(-n);
}

//6

function myFunction(a, b) {
  return a.filter((cur) => cur !== b);
}

//7

function myFunction(a) {
  return a.length;
}

//8

function myFunction(a) {
  return a.filter((el) => el < 0).length;
}

//9

function myFunction(arr) {
  return arr.sort();
}

//10

function myFunction(arr) {
  return arr.sort().reverse();
}

//11

function myFunction(arr) {
  return arr.reduce((a, b) => a + b, 0);
}

//12

function myFunction(arr) {
  return arr.reduce((a, b) => a + b, 0) / arr.length;
}

//13

function myFunction(arr) {
  return arr.reduce((a, b) => (a.length <= b.length ? b : a));
}

//14

function myFunction(arr) {
  return new Set(arr).size === 1;
}

//15

function myFunction(...arrays) {
  return arrays.flat();
}

//16

function myFunction(arr) {
  return arr.sort((x, y) => x.a - y.b);
}

//17
function myFunction(a, b) {
  return [...new Set([...a, ...b])].sort((x, y) => x - y);
}
