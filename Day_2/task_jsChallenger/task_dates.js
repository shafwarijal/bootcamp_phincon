//1
function myFunction(a, b) {
  return a.getTime() == b.getTime();
}

//2

function myFunction(a, b) {
  const dif = Math.abs(a - b);
  return dif / 1000 / 60 / 60 / 24;
}

//3

function myFunction(a, b) {
  return a.getTime() == b.getTime();
}

//4

function myFunction(a, b) {
  return Math.abs((a.getTime() - b.getTime()) / 1000 / 60) <= 60;
}

//5
