//1

function myFunction(a, b) {
  return a + b;
}

//2

function myFunction(a, b) {
  return a === b;
}

//3

function myFunction(a) {
  return typeof a;
}

//4

function myFunction(a, n) {
  return a.charAt(n - 1);
}

//5

function myFunction(a) {
  return a.slice(3);
}

//6

function myFunction(str) {
  return str.slice(-3);
}

//7

function myFunction(a) {
  return a.slice(0, 3);
}

//8

function myFunction(a) {
  return a.indexOf("is");
}

//9

function myFunction(a) {
  return a.slice(0, a.length / 2);
}

//10

function myFunction(a) {
  return a.slice(0, -3);
}

//11

function myFunction(a, b) {
  return (b / 100) * a;
}

//12

function myFunction(a, b, c, d, e, f) {
  return (((a + b - c) * d) / e) ** f;
}

//13

function myFunction(a, b) {
  return a.indexOf(b) === -1 ? a + b : b + a;
}

//14

function myFunction(a) {
  return a % 2 === 0;
}

//15

function myFunction(a, b) {
  return b.split(a).length - 1;
}

//16

function myFunction(a) {
  return a - Math.floor(a) === 0;
}

//17

function myFunction(a, b) {
  return a < b ? a / b : a * b;
}

//18

function myFunction(a) {
  return Number(a.toFixed(2));
}

//19

function myFunction(a) {
  const string = a + "";
  const strings = string.split("");
  return strings.map((digit) => Number(digit));

  // const digits = a.toString().split('');
  // const realDigits = digits.map(Number);
  // return realDigits
}
