//1

function myFunction(obj) {
  return obj.country;
}

//2

function myFunction(obj) {
  return obj["prop-2"];
}

//3

function myFunction(obj, key) {
  return obj[key];
}

//4

function myFunction(a, b) {
  return b in a;
}

//5

function myFunction(a, b) {
  return Boolean(a[b]);
}

//6

function myFunction(a) {
  return { key: a };
}

//7

function myFunction(a, b) {
  return { [a]: b };
}

//8
function myFunction(a, b) {
  return a.reduce((acc, cur, i) => ({ ...acc, [cur]: b[i] }), {});
}

//9
function myFunction(a) {
  return Object.keys(a);
}

//10
function myFunction(obj) {
  return obj?.a?.b;
}

//11
function myFunction(a) {
  return Object.values(a).reduce((x, y) => x + y);
}

//12
function myFunction(obj) {
  delete obj["b"];
  return obj;
}

//13
function myFunction(x, y) {
  y.d = y.b;
  delete y.b;
  return { ...x, ...y };
}

//14
function myFunction(a, b) {
  return Object.entries(a).reduce((acc, [key, value]) => {
    return { ...acc, [key]: value * b };
  }, {});
}
